<!--
SPDX-FileCopyrightText: 2016-2024 Michael Picht <mipi@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

[![REUSE status](https://api.reuse.software/badge/gitlab.com/mipimipi/repman)](https://api.reuse.software/info/gitlab.com/mipimipi/repman)


# nerdstuff.org

This repository contains the content the web site https://nerdstuff.org is generated from. Comments are stored as GitHub discussions in [github.com/mipimipi/nerdstuff.org-comments](https://github.com/mipimipi/nerdstuff.org-comments).
