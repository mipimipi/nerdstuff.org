# SPDX-FileCopyrightText: 2016-2024 Michael Picht <mipi@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM squidfunk/mkdocs-material

RUN pip install \
    mkdocs-material[imaging] \
    mkdocs-rss-plugin 
