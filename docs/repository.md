<!--
SPDX-FileCopyrightText: 2016-2024 Michael Picht <mipi@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

nerdstuff.org provides some packages from the [Arch Linux User Repository](https://aur.archlinux.org) (AUR) for [x86_64](https://en.wikipedia.org/wiki/X86-64) and [AArch64](https://en.wikipedia.org/wiki/AArch64) systems in an own repository - see the content [here](https://getpackages.org/repos/archlinux/nerdstuff/).

To use the nerdstuff repository, add the following lines to your `/etc/pacman.conf` file:

    [nerdstuff]
	Server = https://getpackages.org/repos/archlinux/$repo/$arch

Finally, import the required key by executing

    pacman-key --recv-keys 223AAA374A1D59CE
    pacman-key --finger 223AAA374A1D59CE
    pacman-key --lsign-key 223AAA374A1D59CE

as root.
