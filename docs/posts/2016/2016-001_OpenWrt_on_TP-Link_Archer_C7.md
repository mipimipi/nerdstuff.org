---
title:       Installing OpenWrt on TP-Link Archer C7
authors:     [mipi]
date:
  created:   2016-11-13 
tags:        [openwrt, TP-Link, Archer C7, AC1750]
categories:  [System Installation]
slug:        installing-openwrt-archer-c7
description: >
  How to install OpenWrt on the TP-Link router Archer C7 / AC1750
keywords:    [openwrt, TP-Link, Archer C7, AC1750]
comments:    true
---

# Installing OpenWrt on TP-Link Archer C7

*Wireless freedom* – that’s the appealing slogan of [OpenWrt](https://openwrt.org/), a Linux distribution specifically made for embedded devices such as routers and access points. It gives you full control over what is installed on the device, its customization and can extend the functional capabilities of your router significantly compared to the vendors firmware. But the installation can be quite tricky. In this post I want to share some learnings I made when installing OpenWrt on an Archer C7.

<!-- more -->

## OpenWrt
 
Getting control over what is running on my router and extend its functionality were the main reasons for me looking for an alternative firmware. OpenWrt is not the only software of that kind – there are others like [dd-wrt](https://www.dd-wrt.com/) or [Tomato](http://polarcloud.com/tomato) just to name a few. But compared to them OpenWrt has some advantages:
 
* It supports a lot of different devices. Tomato for instance only supports Broadcom-based devices. 
* It contains a lightweight package manager that allows installing packages for openvpn, samba, ftp etc. etc. from local sources or sources from the internet. With that approach the base software can be kept small and the user is enabled to only  install the packages that are really needed.
* It has a vivid and very active development community. The latest release (Chaos Calmer – version 15.05.1) has come out in March 2016. The last official release of dd-wrt on the other hand is from 2008 as far as I could see.

So, these are all good reasons to go for OpenWrt. Downside is (but that’s true for any other alternative firmware as well) that you lose the warranty – which 
is not really a surprise, of course.

## Archer C7
 
The [TP-Link Archer C7](https://openwrt.org/toh/tp-link/archer-c5-c7-wdr7500) is a wireless router from TP-Link offering a decent performance and low power consumption for a very good price. It supports 2.4 and 5 GHz, 802.11ac and has 4 LAN and 2 USB ports. Currently, you can get it refurbished at ebay for less than 60 Euro. But make sure that you get a device with V2.0 – OpenWrt does not support 5 Ghz on V1.1 devices.
 
## Flashing OpenWrt

The installation process is normally very simple. It is described in the [OpenWrt wiki](https://oldwiki.archive.openwrt.org/toh/tp-link/tl-wdr7500). I did the installation according to method 1 (via OEM firmware). But unfortunately the upgrade process stopped with the message:
                                                                                                                                                          
    Error code: 18005
    Upgrade unsuccessfully because the version of the upgraded file was incorrect. Please check the file name. 

So, it seems that the current TP-Link firmware contains some check that prevents flashing alternative firmwares – sh*t!!! But how to resolve that? After a lengthy and annoying search in diverse forums I found a solution in the [dd-wrt forum](https://forum.dd-wrt.com/phpBB2/viewtopic.php?t=287073&postdays=0&postorder=asc&start=60):

1. Downloads the LEDE firmware from here. LEDE is a fork of OpenWrt.
1. Execute the flashing process via the Web UI of the Archer C7, select the LEDE file that you just have downloaded and update your router. This time error 18005 shouldn’t occur. 
1. Your router is now running with LEDE. Because LEDE does not offer a WebUI you have to access your router via SSH. The device has the IP 192.168.1.1. In order to connect to it via SSH your PC needs to be in the same subnet (i.e. it has to have the IP 192.168.1.x). You could give it e.g. the static IP 192.168.1.2. Now connect the LAN port of your PC to one of the four LAN ports of the C7. Connect another LAN port of the C7 to your local network (a switch, another router or whatwever you have). Restart the network manager of your PC. Now, it should have the IP 192.168.1.2 and a ping 192.168.1.1 should be possible.
1. Access the C7 via SSH: ssh root@192.168.1.1. Type yes and press enter. When it asks for the password just press enter because the initial password is empty.
1. Now you can install the WebUI. First execute opkg update which updates the software on your device. The install the Web UI via opkg install luci-ssl. Once this is finished the WebUI is installed and you should be able to access it from the browser via http://192.168.1.1. Use user root and empty password.
1. Now you can install OpenWrt. Download the current version of OpenWrt as described in the OpenWrt wiki. In the LEDE WebUI navigate to System – Backup / Flash Firmware. There, under Flash new firmware image select the OpenWrt image you have download and flash it.
1. After this is done you can access the OpenWrt WebUI via http://192.168.1.1. Login with user root and password empty. Now you can configure OpenWrt. First you should define a root password and give the router an IP from you desired subnet. Once this is done you can disconnect the C7 from your PC and reconnect the PC and the C7 to your local network. You should now be able to acces the C7 via WebUI under the new IP.
1. Configure OpenWrt according to your needs and have fun with it.
