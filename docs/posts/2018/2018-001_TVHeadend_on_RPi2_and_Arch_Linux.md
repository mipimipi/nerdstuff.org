---
title:       Installing TVHeadend on RPi2 with Arch Linux
authors:     [mipi]
date:
  created:   2018-07-27
  updated:   2021-10-15
tags:        [arch linux, arch linux arm, tvheadend, raspberry pi]
categories:  [Arch Linux, System Installation, ARM]
slug:        installing-tvheadend-arch-raspberry-pi
description: >
  How to install a TV server with TVHeadend and Arch Linux ARM on a Raspberry Pi 2
keywords:    [arch linux, arch linux arm, tvheadend, raspberry pi]
comments:    true
---

# Installing TVHeadend on RPi2 with Arch Linux

A Raspberry Pi is perfectly suited to run a [TVHeadend](https://tvheadend.org/) server - for receiving Live TV, for example. In this post I explain, how such a server can be run on an RPi2 with [Arch Linux ARM](https://archlinuxarm.org/).

<!-- more --> 

I would like to receive Live-TV via [DVB-T2](https://en.wikipedia.org/wiki/DVB-T2) and stream it into my LAN. As streaming server I would like to use [TVHeadend](https://tvheadend.org/). In my setup, the DVB-T2 signal is received by an antenna, which is connected to an USB tuner, which in turn is connected to the RPi. The RPi is connected to the LAN and streams the Live-TV.

Often, [Raspbian](https://www.raspbian.org/) is used as OS for a RPi, but since I am a fan of [Arch Linux](https://www.archlinux.org/) I wanted to use this as basis for my server. For this project I use the [ARM port of Arch Linux](https://archlinuxarm.org/). In a nutshell, the following steps need to be executed:

1. Install Arch Linux ARM
1. Configure Arch Linux ARM
1. Install Yay to be able to install packages from the [Arch Linux User Respository](https://aur.archlinux.org) (AUR)
1. Install TVHeadend from AUR
1. Configure TVHeadend and run it as service

I use an RPi model 2, but this instruction could also be used to install Arch Linux ARM and TVHeadend on other computers with an ARM board. Just download the appropriate version of Arch Linux ARM and follow the corresponding installation guidelines for that version.

This how-to only contains the steps to set up the TVHeadend server. Since the configuration of DVB-T2 in TVHeadend is depending on the country, it is not covered here. A good instruction for Germany is provided in the [Arch Linux wiki](https://wiki.archlinux.org/title/Tvheadend#DVB-T2_HD_in_Germany)[^1].

Since I was asked which hardware besides the RPi I am using: As USB TV tuner I use the [DVBSky T330 receiver](http://www.dvbsky.net/Products_T330.html) which is currently sold in a bundle with a small antenna. The USB receiver is recognized by Arch Linux ARM out-of-the-box, but it needs firmware moduls in addition. They can be downloaded from the [manufacturers' web site](https://web.archive.org/web/20230113021146/http://www.dvbsky.net/Support_linux.html)[^2]. After you went through the entire installation instruction below, just unzip the downloaded firmware archive and copy the `*.fw` files via `scp` to the RPi to the directory `/lib/firmware`. Reboot and you should be done. 

## Install Arch Linux on Raspberry Pi 2

According to [this instruction](https://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2)[^3], Arch Linux ARM will be installed on the RPi2. Replace `sdX` in the described steps with the device name for the SD card as it appears on your computer.

Start fdisk to partition the SD card:

    fdisk /dev/sdX

At the fdisk prompt, delete old partitions and create a new one:

1. Type `o`. This will clear out any partitions on the drive.
1. Type `p` to list partitions. There should be no partitions left.
1. Type `n`, then `p` for primary (that's the default), `1` for the first partition on the drive (that's also the default), press ENTER to accept the default first sector, then type `+100M` for the last sector.
1. Type `t`, then `c` to set the first partition to type `W95 FAT32 (LBA)`.
1. Type `n`, then `p` for primary, `2` for the second partition on the drive, and then press ENTER twice to accept the default first and last sector.
1. Write the partition table and exit by typing `w`.

Format and mount the boot partition:
    
    mkfs.vfat /dev/sdX1
    mkdir boot
    mount /dev/sdX1 boot

Format and mount the root partition:

    mkfs.ext4 /dev/sdX2
    mkdir root
    mount /dev/sdX2 root

Download and extract the root filesystem:

    wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz
    bsdtar -xpf ArchLinuxARM-rpi-2-latest.tar.gz -C root
    sync

Move boot files to the boot partition:

    cp -r root/boot/* boot/.
    rm -rd root/boot/*

Unmount the two partitions:

    umount boot root

## Configure Arch Linux on RPi2

Insert the SD card into the Raspberry Pi, connect ethernet, and apply 5V power. Use SSH to the IP address given to the board by your router. Login as the default user `alarm` with password `alarm` (the default root password is `root`).

    ssh alarm@<ip-address>

### Configure Pacman and upgrade system

Initialize the pacman keyring and populate the Arch Linux ARM package signing keys:

    su
    pacman-key --init
    pacman-key --populate archlinuxarm

Upgrade system:

    pacman -Syu

### Enable Sudo

Install `sudo`:

    pacman -S sudo

Configure `sudo`:

    nano /etc/sudoers

Remove the comment sign in front of the row

    %wheel      ALL=(ALL) ALL

Save the file (CTRL+O, ENTER) and exit (CTRL+X).    

To allow user `alarm` to use `sudo`, add the user to group `wheel`:

    gpasswd --add alarm wheel

### Configure network

#### Set static ip address

Adjust file `/etc/systemd/network/eth0.network` (replace `eth0` with your interface):

    nano /etc/systemd/network/eth0.network

Remove line

    DHCP=yes

Add the lines (replace the ip addresses with your addresses):

    Address=192.168.0.100/24
    Gateway=192.168.0.1
    DNS=192.168.0.1

Save the file (CTRL+O, ENTER) and exit (CTRL+X). 

#### Set host name

Edit file `/etc/hostname`:

    nano /etc/hostname

Replace the default host name with your host name. Save the file (CTRL+O, ENTER) and exit (CTRL+X).

Besides that, add the row

    127.0.0.1 localhost

to `/etc/hosts`.    

### Reboot

Reboot the system:

    reboot

### Install Yay

This installation instruction is taken from the [Yay README](https://github.com/Jguer/yay)[^4].

Login as default user via SSH (now, you have to use the static ip address of the RPi):

    ssh alarm@<static-ip-address>

#### Install Yay binary from AUR

    sudo pacman -S --needed git base-devel
    git clone https://aur.archlinux.org/yay-bin.git
    cd yay-bin
    makepkg -si

### Install and set up TVHeadend

Install TVHeadend (this takes a while):

    yay -S tvheadend-git

I used `tvheadend-git` instead of the stable version `tvheadend` since the stable version could not be built when I did the installation. `tvheadend-git` worked without problems.    

Set up TVHeadend as service and start it:

    sudo systemctl enable tvheadend.service --now

You should now be able to access the browser UI of TVHeadend via the URL `http://<static-ip-address>:9981`.

Have fun with TVHeadend!

## References

[^1]: [How to scan DVB-T2 HD in Germany](https://wiki.archlinux.org/title/Tvheadend#DVB-T2_HD_in_Germany)
[^2]: [Firmware for DBVSky USB Tuner](https://web.archive.org/web/20230113021146/http://www.dvbsky.net/Support_linux.html) (Internet Archive)
[^3]: [How to install Arch Linux ARM on Raspberry Pi 2](https://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2)
[^4]: [How to install Yay](https://github.com/Jguer/yay)

