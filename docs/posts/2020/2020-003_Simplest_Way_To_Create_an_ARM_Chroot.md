---
title:       The Simplest Way To Create an ARM Chroot in Arch Linux
authors:     [mipi]
date:
  created:   2020-04-25
  updated:   2024-01-14
tags:        [arch linux, x86_64, arm, qemu, chroot, raspberry pi, arch linux arm]
categories:  [Arch Linux, ARM]
slug:        creating-arm-chroot-in-arch
description: >
  How to create a chroot environment for an ARM architecture on an x86_64 Arch Linux system
keywords:    [arch linux, x86_64, arm, qemu, chroot, raspberry pi, arch linux arm]
comments:    true
---

Utilizing an [ARM](https://en.wikipedia.org/wiki/ARM_architecture) chroot on a [x86_64](https://en.wikipedia.org/wiki/X86-64) system together with [QEMU](https://www.qemu.org/) is a rather easy and convenient way to build ARM packages on a x86_64 machine. In this post, we describe the simplest way in doing so on [Arch Linux](https://archlinux.org).

<!-- more -->

The Arch Linux wiki [describes an approach to create an ARM chroot](https://wiki.archlinux.org/index.php/QEMU#Chrooting_into_arm/arm64_environment_from_x86_64) leveraging [QEMU](https://www.qemu.org/). It assumes that an ARM root partition is installed on an SD card/storage. This storage is mounted to a folder on an Arch Linux (x86_64) system and used as root for the [chroot](https://wiki.archlinux.org/index.php/Chroot) command.

That's a quite simple solution if you have a real ARM device but do not want to log into it device to install software. But if you just want to build ARM packages, you don't even need a real device or an SD card. Instead one can use image archives from [Arch Linux ARM](https://archlinuxarm.org), extract such an archive into a folder and chroot into that folder to build such packages.

The first steps are identical to those described in the Arch Linux wiki:

1. Install [qemu-user-static-binfmt](https://archlinux.org/packages/extra/x86_64/qemu-user-static-binfmt/) and [qemu-user-static](https://archlinux.org/packages/extra/x86_64/qemu-user-static/) on the x86_64 host.
1. Restart the systemd-binfmt service:
 
        systemctl restart systemd-binfmt.service

1. Make sure that the ARM executable support is active for the desired ARM architecture by execute

        ls /proc/sys/fs/binfmt_misc

    and verifying that the architecture is in the list.

Instead of mounting an SD card, we use an image from Arch Linux ARM:

1. Download an image file from [Arch Linu ARM](https://archlinuxarm.org)
1. Create a folder that you want to use as root for the chroot environment.
1. Extract the image files from the archive into that folder:

        bsdtar -xpf <image-archive> -C <root-folder>

1. Make the new root folder a mount point:

        mount --bind <root-folder> <root-folder>

1. Chroot into the new folder, initialize the pacman keyring and populate the Arch Linux ARM package signing keys:

        arch-chroot <root-folder>
        pacman-key --init
        pacman-key --populate archlinuxarm

1. Update the chroot environment and use it:

        pacman -Syu

