---
title:       Automating the Installation of Arch Linux Systems
authors:     [mipi]
date: 
  created:   2020-02-16
tags:        [arch linux, installation, automation, meta packages]
categories:  [Arch Linux, System Installation, Package Management]
slug:        automating-arch-installation 
description: >
  Different strategies and approaches to make the installation of Arch Linux more
  convenient and easier to repeat
keywords:    [arch linux, installation, automation, meta packages]
comments:    true
---

[Arch Linux](https://www.archlinux.org/) is great because it brings flexibility and gives users control since it's their decision how the system is set up and configured. On the other hand, the [standard installing process](https://wiki.archlinux.org/index.php/installation_guide)[^1] is completely manual, and if the installation needs to be done more often or if multiple machines need to be installed, the process can become cumbersome and annoying. In this post, we will discuss some approaches to make the installation more convenient and easier to repeat.

<!-- more -->

The installation of an Arch Linux system can be divided into three steps:

1. Installation of the Arch Linux base system
1. Installation of additional software packages and its global (i.e., user-independent) configuration
1. Personal (i.e., user-specific) configuration

Let's take a closer look at each of these steps:

## 1. Installation of the Arch Linux Base System

This step typically comprises the activities that are mentioned in the [Arch Linux installation guide](https://wiki.archlinux.org/index.php/installation_guide), such as in the partitioning of disks, formatting and mounting of partitions, configuration of fstab, time zone, localization etc. 

There are different possibilities to make this more convenient:

### Arch-based Distribution with Live Mode

Instead of booting the system to be installed from the [Arch Linux ISO](https://www.archlinux.org/download/) into a root console, an [Arch-based distribution](https://wiki.archlinux.org/index.php/Arch-based_distributions) with a [live mode](https://en.wikipedia.org/wiki/Live_CD) could be used - not all distributions offer this possibility.

To install Arch Linux, you boot the machine into the live mode of the distribution, open a browser window with the installation instruction of Arch Linux and - in parallel - a terminal window where you actually do the installation. With this approach, you have the installation instruction and the terminal side by side and can copy and paste commands. That's quite convenient and keeps the full control in your hands.

### Installation Scripts

There are various scripts out there, that provide a guided, menue-based installation process. Some examples:

* [Archlinux U Install](https://github.com/helmuthdu/aui)
* [arch-installer](https://github.com/rstacruz/arch-installer)
* [archfi](https://github.com/MatMoul/archfi)

To install Arch Linux with such a script, the machine is normally booted from the Arch Linux ISO. The installation script is then downloaded via `curl` or `wget`.

While the installation is easier than with the already mention approach, the flexibility is often limited. Most of these installers have restrictions, sometimes they to not support [encryption](https://wiki.archlinux.org/index.php/disk_encryption) or a partition schema with [LVM](https://wiki.archlinux.org/index.php/LVM), sometimes they do not support the [boot manager / boot loader](https://wiki.archlinux.org/index.php/Category:Boot_loaders) that you want etc. Thus, using such installer is a trade-off between convenience and flexibility.

## 2. Installation of Additional Software Packages

This step is about installing additional software packages to the base system, such as a (graphical) [desktop environment](https://wiki.archlinux.org/index.php/Desktop_environment#Use_a_different_window_manager) (DE) and other applications one typically needs (a web browser, a mail client, maybe an [IDE](https://wiki.archlinux.org/index.php/List_of_applications#Integrated_development_environments), and so on and so forth). Maybe you want to use one of the bundled DE's, such as [Gnome](https://wiki.archlinux.org/index.php/GNOME), [KDE/Plasma](https://wiki.archlinux.org/index.php/KDE) or [Xfce](https://wiki.archlinux.org/index.php/xfce), or maybe you want to bundle your own [custom DE](https://wiki.archlinux.org/index.php/Desktop_environment#Custom_environments). In addition to the pure installation of the software, configuration and theming is required, but also the enablement of ([systemd](https://wiki.archlinux.org/index.php/systemd)) services. 

[Meta packages](https://wiki.archlinux.org/index.php/Meta_package_and_package_group) can take care of all this. They are Arch Linux packages that depend on other packages. The installation of a meta packages triggers the installation of all packages that the meta package depends on. Thus, a meta package can be used to install a specific set of Arch Linux packages. Furthermore they can contain logic to create or modify configuration files or to enable services. To make a meta package available for installation, a [custom repository](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Custom_local_repository) is a good means. More details about meta packages will be covered in a dedicated post. So far, [this](https://disconnected.systems/blog/archlinux-meta-packages/)[^2] might help. 

## 3. Personal Configuration

The user-specific configuration is typically done via [dotfiles](https://wiki.archlinux.org/index.php/Dotfiles). They can be managed and transferred between systems with [dotfile managers](https://wiki.archlinux.org/index.php/Dotfiles#Tools).

## Summing It Up

With the presented approaches, the overall installation process would be as follows:

### Preparation

1. Prepare the meta package(s) and make them available via a [custom repository](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Custom_local_repository).
1. Download the ISO that you want to use for the installation - either the [Arch Linux ISO](https://www.archlinux.org/download/) or an Arch-based distribution with live mode.

### Installation

1. Boot your machine from the ISO media.
1. Install the base system either via the [standard installation guide](https://wiki.archlinux.org/index.php/installation_guide) or via a 3rd-party installation script.
1. Reboot your machine. Add your custom repository to `/etc/pacman.conf` and install the meta package via `pacman -Syu <your-meta-package>`
1. Reboot your machine. Create a user (provided, this hasn't been done before), and create the user-specific configuration with a dotfile manager
1. Have fun with your new system

## References

[^1]: [Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/installation_guide)
[^2]: [Managing Arch Linux with Meta Packages](https://disconnected.systems/blog/archlinux-meta-packages/)
