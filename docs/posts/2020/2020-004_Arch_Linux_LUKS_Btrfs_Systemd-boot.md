---
title:       Installing Arch Linux with Btrfs, systemd-boot and LUKS
authors:     [mipi]
date:
  created:   2020-05-31
  updated:   2022-03-12
tags:        [arch linux, btrfs, encryption, installation, UEFI]
categories:  [Arch Linux, System Installation]
slug:        installing-arch-btrfs-luks
description: >
  Installation of an encrypted system with Arch Linux, Btrfs and systemd-boot
keywords:    [arch linux, btrfs, encryption, installation, UEFI]
comments:    true
---

My preferred Arch Linux setup is with [Btrfs](https://wiki.archlinux.org/index.php/Btrfs), [systemd-boot](https://wiki.archlinux.org/index.php/Systemd-boot) (which requires [UEFI](https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface)) and - at least when installing a laptop - [LUKS](https://gitlab.com/cryptsetup/cryptsetup). Not least for myself, to make the installation easier the next time, I thought that it's a good idea to document the steps.

<!-- more -->

!!! note

	This post describes the creation and usage of a swap file, but that's optional.

## Using SSH

If you have another PC, it's convenient to do the installation from this PC via [SSH](https://wiki.archlinux.org/index.php/OpenSSH) because then you can have browser windows with installations guides open in parallel and easily copy and paste commands. To do so, boot the PC that you want to install from the [Arch Linux ISO](https://www.archlinux.org/download/).

Start SSH:

    systemctl start sshd.service

Set a password for root:

    passwd

Look up the IP address of the PC:

    ip addr show

Now, on the second PC, call into the first PC via SSH:

    ssh root@<IP-OF-THE-FIRST-PC>

and continue with the installation.

## Partitioning

!!! note

	For the rest of this post, we assume that we install Arch Linux on `/dev/sda`.
	
Adjust the steps for your setup if necessary.

    gdisk /dev/sda

Create new partition table:

    Command (? for help): o

Create an EFI partition (choose size 550M and hex code `EF00`):

    Command (? for help): n

Create a root partition (adopt the default values):

    Command (? for help): n

Write the new partitions to disk:

    Command (? for help): w

## Encryption

Create an encrypted container for the root file system (you need to define a passphrase):

    cryptsetup luksFormat /dev/sda2

Open the container ("`luks`" is just a placeholder, you can use a name of your choice, but remember to adopt the subsequent steps of the guide accordingly):

    cryptsetup open /dev/sda2 luks

## File System Creation

Format the EFI partition with FAT32 and give it the label `EFI` - you can choose any other label name:

    mkfs.vfat -F32 -n EFI /dev/sda1

Format the root partition with Btrfs and give it the label `ROOT` - you can choose any other label name. If you didn't open the LUKS container under the name "`luks`" you must adjust the command accordingly:

    mkfs.btrfs -L ROOT /dev/mapper/luks

## Create and Mount Sub Volumes

!!! note

	If you don't want to use a swap file, it is not required to create and mount the sub volume `@swap`.

Create [sub volumes](https://wiki.archlinux.org/index.php/Btrfs#Subvolumes) for root, home, the package cache, [snapshots](https://wiki.archlinux.org/index.php/Btrfs#Snapshots) and the entire Btrfs file system:

    mount /dev/mapper/luks /mnt
    btrfs sub create /mnt/@
    btrfs sub create /mnt/@swap
    btrfs sub create /mnt/@home
    btrfs sub create /mnt/@pkg
    btrfs sub create /mnt/@snapshots
    umount /mnt

Mount the sub volumes:

    mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@ /dev/mapper/luks /mnt
    mkdir -p /mnt/{boot,home,var/cache/pacman/pkg,.snapshots,btrfs}
    mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@home /dev/mapper/luks /mnt/home
    mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@pkg /dev/mapper/luks /mnt/var/cache/pacman/pkg
    mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvol=@snapshots /dev/mapper/luks /mnt/.snapshots
    mount -o noatime,nodiratime,compress=zstd,space_cache=v2,ssd,subvolid=5 /dev/mapper/luks /mnt/btrfs

Mount the EFI partition

    mount /dev/sda1 /mnt/boot

Create swap file (taken from [Btrfs - Swap file](https://wiki.archlinux.org/title/Btrfs#Swap_file) and [Swap file](https://wiki.archlinux.org/title/Swap#Swap_file)):

    cd /mnt/btrfs/@swap
    truncate -s 0 ./swapfile
    chattr +C ./swapfile
    btrfs property set ./swapfile compression none
    dd if=/dev/zero of=./swapfile bs=1M count=<FILE-SIZE-IN-MiB> status=progress
    chmod 600 ./swapfile
    mkswap ./swapfile
    swapon ./swapfile
    cd -

## Base System and `/etc/fstab`

Install Arch Linux (adjust this list to your needs):

    pacstrap /mnt linux linux-firmware base btrfs-progs intel-ucode nano

Generate `/etc/fstab`:

    genfstab -U /mnt >> /mnt/etc/fstab

## System Configuration

Since I want a German system the following steps are according to that. Adjust them to your needs.

`chroot` into the new system:

    arch-chroot /mnt/

Set host name:

    echo <YOUR-HOSTNAME> > /etc/hostname

Set locale:

    echo LANG=de_DE.UTF-8 > /etc/locale.conf

Uncomment the following rows of `/etc/locale.gen`:

    #de_DE.UTF-8 UTF-8
    #de_DE ISO-8859-1
    #de_DE@euro ISO-8859-15

Generate locale:

    locale-gen

Set keyboard layout and font:

    echo KEYMAP=de-latin1 > /etc/vconsole.conf
    echo FONT=lat9w-16 >> /etc/vconsole.conf

Set time zone:

    ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

Define hosts in `/etc/hosts`:

    #<ip-address>	<hostname.domain.org>	<hostname>
    127.0.0.1	<YOUR-HOSTNAME>.localdomain	<YOUR-HOSTNAME>
    ::1		localhost.localdomain	localhost

Set root password:

    passwd

### Initramfs

Configure the creation of [initramfs](https://wiki.archlinux.org/index.php/Arch_boot_process#initramfs) by editing `/etc/mkinitcpio.conf`. Change the line `HOOKS=...` to:

    HOOKS=(base keyboard udev autodetect modconf block keymap encrypt btrfs filesystems resume)

!!! note

	If you don't use a swap file, leave out the `resume` hook.

Recreate initramfs:

    mkinitcpio -p linux

### Boot Manager

Install [systemd-boot](https://wiki.archlinux.org/index.php/Systemd-boot):

    bootctl --path=/boot install

Create file `/boot/loader/entries/arch.conf` and fill it with:

    title Arch Linux
    linux /vmlinuz-linux
    initrd /intel-ucode.img
    initrd /initramfs-linux.img
    options cryptdevice=UUID=<UUID-OF-ROOT-PARTITION>:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=@ rd.luks.options=discard rw resume=/dev/mapper/luks resume_offset=<YOUR-OFFSET>

To calculate the offset for the swap file, follow the instructions from [Hibernation into swap file on Btrfs](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Hibernation_into_swap_file_on_Btrfs).

!!! note

	If you don't use a swap file, leave out the options `resume` and `resume_offset`.

The UUID of the root partition can be determined via:

    blkid -s UUID -o value /dev/sda2

Edit file `/boot/loader/loader.conf` and fill it with:

    default  arch.conf
    timeout  4
    console-mode max
    editor   no

### Final Steps

Exit `chroot`, unmount partitions and reboot:

    exit
    umount -R /mnt
    reboot

Have fun with your system!

## References

- [Arch My Way 1 | Grundsystem installieren](https://www.youtube.com/watch?v=oT7gs2CmsnQ) (German) is a nice instruction about an Arch Linux installation with Btrfs
- [Arch Linux - UEFI, systemd-boot, LUKS, and btrfs](https://austinmorlan.com/posts/arch_linux_install/) is a helpful blog post about an Arch Linux installation with systemd-boot, LUKS and Btrfs
